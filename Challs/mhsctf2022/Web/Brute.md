#Challenge de web
#Et tu, Brute?(Web EXploit)
(https://mhsctf2022.ctfd.io/challenges#Et%20tu,%20Brute?-35 --24 Février 2022)
#Solutions
Solution(s) externe(s):0
Propre Solution:1
On devait trouver deux nombres. Pour cela il faut faire du bruteforce. Pour cela on peut utiliser burpsuite ou owasp-zap. Cependant zap est très rapide et mieux. Il faut ouvrir zap. Ensuite ouvrir le navigateur de zap. Après il faut ouvrir le lien du site dans ce navigateur et taper des valeurs quelconques puis valider et intercepter la requête dans zap. Après il faudrait ajouter de payloads pour chaque variable et lancer la requête. A la fin il faut chercher la combinaison dont la tailles en octets ou autre chose diffère des autres. C'est probalement cette combinaison la bonne.

