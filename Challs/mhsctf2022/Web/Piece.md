#Challenge de web
#Piece It Together(Web Exploit)
(https://mhsctf2022.ctfd.io/challenges#Piece%20It%20Together-20 --21 Février 2022) --Moyen
#Solutions 
SOlution(s) externes(s):0
Propre solution:1
Ici on devait remplir un formulaire dont nous ne connaissons l'information. Par ailleurs le code source a été obfusqué. Pour ce faire il faut inspecter le code, copier le code obfusquer et utiliser un outil déobfuscateur comme le site dcode pour deobfusquer le code et obtenir un code clair. POur finir il faut se servir de sa maîtrise du langage JavaScript pour retrouver le flag qui est très évident dans le code.
