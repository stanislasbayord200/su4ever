#Challenge de Web
#Bend(Web Exploit)
(https://mhsctf2022.ctfd.io/challenges#Bend-17 --21 Février 2022)-- Moyen
#Solutions
Solution(s) externe(s):0
Propre solution(s):1
Ici on devait trouver le flag en cliquant sur un lien. Mais lorsqu'on clique sur le lien on est rédirigé sur une video de Rick Astley (Never Gonna give up). De cette video a été inspiré le rickroll qui consiste à rédiriger la cible vers quelque chose d'autre qu'elle n'attendait pas. Pour trouver le flag , on fait un clique droit sur le site et on clique sur inspecter le code. Après on clique sur le lien. Ensuite on se rend dans la section network de la barre d'inspection0. A ce niveau il faut bloquer le lien vers youtube. Après cela on fait un retour sur la page du lien et on regarde le code source. Le flag s'y trouve.
