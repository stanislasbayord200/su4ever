#Challenge de Forensic
#Chimera(Forensic)
(https://mhsctf2022.ctfd.io/challenges#Chimera-19 --24 Février 2022)--Moyen
#Solutions
Solution(s) externe(s):0
Propre solutions:1
On a fichier image sur lequel se trouve le flag. Aller sur le site cyberchef. Aller dans Forensic et choisir <extract file> tout en décochant tout sauf image. Importer l'image vous trouverez le flag sur l'image en sortie.
