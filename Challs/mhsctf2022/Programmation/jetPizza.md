# Programmation
## Jet's Pizza
___
Voici le programme soumis :
```
def main():
    prices = {'T':1.5, 'O':1.25, 'P':3.5, 'M':3.75, 'A':0.4}

    for i in range(7):
        total = 15.00
        toppings = list(set(input()))
        for char in toppings:
            if char in prices.keys():
                total += prices[char]
        if total > 20:
            total -= total * 5 / 100
        print('%.2f' %(round(total, 2)))
            
main()
```