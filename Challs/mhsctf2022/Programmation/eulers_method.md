# Programmation
## Euler's Method

Après quelques recherches sur l'internet, j'ai trouvé une [page](https://www.freecodecamp.org/news/eulers-method-explained-with-examples/) qui résumait assez bien la méthode.
Alors j'ai adapté la fonction trouvé sur cette page au contexte du challenge en maintenant x0 comme 5 et y0 comme 2.
Apres avoir constater que le nombre de pas doit être une valeur entière, j'ai fais d'autre recherche à ce propos et j'ai finalement trouvé la formule suivante pour calculer le nombre de pas à partir d'**un nombre de pas décimal**
> n = (x - y0) / nbPasDec (avec x la valeur à approchée)

```
def main():
	nbAddLines = int(input())

	for i in range(nbAddLines):
		x0 = 5
		y0 = 2
		elements = [float(j) for j in input().split()]
		xn = elements[1]
		n = (elements[1] - 5) / elements[0]
		n = round(n)
		
		h = (xn - x0) / n
		for i in range(n):
			slope = x0**2 - 6 * y0**2
			yn = y0 + h * slope
			y0 = yn
			x0 = x0 + h
		print('%.1f' %(yn))

main()
```
	