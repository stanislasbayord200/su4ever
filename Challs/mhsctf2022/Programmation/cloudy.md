# Programmation
## Cloudy w/ a chance to rain
___
Pour résoudre ce challenge, je me suis basé sur le principe de la probabilité selon laquelle **la probabilité qu'un évenement arrive sur plusieurs intervalle est égale à la probabilité qu'elle arrive sur l'un des intervalles et non sur les autres ou sur deux des intervalles et non sur les autres ou . . .**  
J'ai suivit ce dernier principe en calculant la probabilité qu'il pleuve sur chaque combinaison d'heures allant de 1 à 6
```
def multi(tableau):
	total = 1
	for i in tableau:
		total *= i
	return total

def main():

	nbFois = int(input())
	for it in range(nbFois):
		tabPourcent = [int(entier) for entier in input().split()]
		tab = [i / 100 for i in tabPourcent]
		tabInv = [1 - i for i in tab]
		taille = len(tab)

		proba = 0

		for i in range(taille):
			newTable = [i for i in tabInv]
			newTable[i] = tab[i]
			proba += multi(newTable)


		for i in range(taille - 1):
			for j in range(i + 1, taille):
				newTable = [i for i in tabInv]
				newTable[i] = tab[i]
				newTable[j] = tab[j]
				proba += multi(newTable)

		for i in range(taille - 2):
			for j in range(i + 1, taille - 1):
				for k in range(j + 1, taille):
					newTable = [i for i in tabInv]
					newTable[i] = tab[i]
					newTable[j] = tab[j]
					newTable[k] = tab[k]
					proba += multi(newTable)

		for i in range(taille - 3):
			for j in range(i + 1, taille - 2):
				for k in range(j + 1, taille - 1):
					for l in range(k + 1, taille):
						newTable = [i for i in tabInv]
						newTable[i] = tab[i]
						newTable[j] = tab[j]
						newTable[k] = tab[k]
						newTable[l] = tab[l]
						proba += multi(newTable)

		for i in range(taille - 4):
			for j in range(i + 1, taille - 3):
				for k in range(j + 1, taille - 2):
					for l in range(k + 1, taille - 1):
						for m in range(l + 1, taille):
							newTable = [i for i in tabInv]
							newTable[i] = tab[i]
							newTable[j] = tab[j]
							newTable[k] = tab[k]
							newTable[l] = tab[l]
							newTable[m] = tab[m]				
							proba += multi(newTable)

		for i in range(taille - 5):
			for j in range(i + 1, taille - 4):
				for k in range(j + 1, taille - 3):
					for l in range(k + 1, taille - 2):
						for m in range(l + 1, taille - 1):
							for n in range(m + 1, taille):
								newTable = [i for i in tabInv]
								newTable[i] = tab[i]
								newTable[j] = tab[j]
								newTable[k] = tab[k]
								newTable[l] = tab[l]
								newTable[m] = tab[m]
								newTable[n] = tab[n]
								proba += multi(newTable)
		print(int(proba * 100))

main()
``` 