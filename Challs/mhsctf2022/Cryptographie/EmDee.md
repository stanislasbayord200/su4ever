# Cryptographie
## Em Dee
___

J'ai remarqué que tout les mots cryptés avait tous les mêmes taille donc j'ai pensé à l'algorithme de hashage md5.  
Mais pour m'en rassurer, j'ai écris un programme qui m'a permis de voir quel hash étais utilisé pour.  
Finalement, il s'agissait bien du md5


### + hash_fonction
```
import hashlib
class hashit:
    def hashing(self, texte, hash_type):
        texte=texte.encode('utf-8') #Encoder le texte avant de hacher
        hash_1=hashlib.new(hash_type)  #On choisit le type de hachage qu'on veut utiliser
        hash_1.update(texte);   #On effectu le type de hachage choisit sur notre texte
        return hash_1.hexdigest()   #Retourne la valeur de notre texte apres la transformation en chiffre hèxadecimaux
        
```


### + decrypt.py
```
import hashlib
from hash_fonction import hashit

mot = ['happy', 'sad']
hash = ['56ab24c15b72a457069c5ea42fcfc640', '49f0bad299687c62334182178bfd75d8']
Hash = hashit()
list_algo = hashlib.algorithms_available

for algo in list_algo:
    print(algo, ":", Hash.hashing(mot[0], algo))
    if (hash[0] == Hash.hashing(mot[0], algo)) and (hash[1] == Hash.hashing(mot[1], algo)):
        print("Algorithme :", algo)
```

J'ai donc pris le mot chiffré que j'ai ensuite déchiffrer avec une ressource sur le net.
Ce qui s'est finalement révélé être le flag.