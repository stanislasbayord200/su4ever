#Challenge de Cryptographie
#Crash Hacker (Em Dee 2) (Cryptographie)
(https://mhsctf2022.ctfd.io/challenges#Crash%20Hacker%20(Em%20Dee%202)-9 --21 Février 2022) --Facile
#Solutions 
Solution(s) externe(s):0
Propre solution:1
On devait trouver le flag dans un message haché.
En utilisant hash buster on retrouve le flag.
La commande est : buster -s <hash>
