#Challenge d'ingenierie inverse
#Avengers Assemble 2(Reverse Engineering)
(https://mhsctf2022.ctfd.io/challenges#Avengers%20Assemble%202-37 --24 Février 2022)--Difficile
#Solutions
Solution(s) externe(s):0
Propre Solutions:1
Nous avons un fichier avec du code assembleur. En déchiffrant ce code vous trouverez les sorties des variables c et d sans oublier la valeur de retour eax. Pour cela il faut connaitre le langage assembleur.
